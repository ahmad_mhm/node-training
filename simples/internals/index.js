const request = require('./request')
// const response = require('./response')

module.exports = {
    // request: require('./request'),
    // response: require('./response'),
    send: request.send,
    // read: response.read,
    ...require('./response')
}
