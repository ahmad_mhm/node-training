const EventEmitter = require('events')
const celebrity = new EventEmitter()

//Subscribe to celebrity for observer 1
celebrity.on("ahmad", (state) => {
    if (state === "saying")
        console.log("congratulations, You are the best!")
})

//Subscribe to celebrity for observer 2
celebrity.on("ahmad", (state) => {
    if (state === "saying")
        console.log("Boo I could have better than that!")
})

process.on('exit', (code) => {
    console.log("ahmad shuting down the saying with code: ", code)
})

celebrity.emit("ahmad","saying")
celebrity.emit("ahmad dont say")
celebrity.emit("ahmad","saying")