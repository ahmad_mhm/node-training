const { send } = require('./request')
// const response = require('./response')
const { read } = require('./response')

function makeRequest(url, data) {
    send(url, data);
    return read();
}

const responseData = makeRequest('www.google.com', 'Hello')
console.log(responseData)