// const internals = require('./internals')
const { send, read } = require('./internals')
function makeRequest(url, data) {
    // internals.request.send(url, data);
    // internals.send(url, data)
    send(url, data)
    return read();
}

const responseData = makeRequest('www.google.com', 'Hello')
console.log(responseData)